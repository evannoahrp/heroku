"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    static associate(models) {}
  }
  Article.init(
    {
      title: DataTypes.STRING,
      body: DataTypes.TEXT,
      approved: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Article",
    }
  );
  return Article;
};
