module.exports = {
  index: (req, res) => {
    const title = "Home";
    res.render("home", { title });
  },
};
