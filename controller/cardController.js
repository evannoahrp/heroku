const { response } = require("express");
const { Card } = require("../models");

module.exports = {
  create: (req, res) => {
    const { href, content } = req.body;
    Card.create({
      href,
      content,
    })
      .then((card) => {
        res.json({
          status: 200,
          message: "Post berhasil",
          data: card,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  index: (req, res) => {
    const title = "Cards";
    Card.findAll({})
      .then((card) => {
        if (card !== 0) {
          res.render("card", { title, card });
        } else {
          res.json({
            status: 400,
            message: "Data tidak ditemukan",
          });
        }
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  show: (req, res) => {
    const cardId = req.params.id;
    Card.findOne({
      where: {
        id: cardId,
      },
    })
      .then((card) => {
        res.json({
          status: 200,
          message: "Berhasil",
          data: card,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  getCreate: (req, res) => {
    const title = "create";
    res.render("create", { title });
  },

  update: (req, res) => {
    const cardId = req.params.id;
    const { href, content } = req.body;
    Card.update(
      {
        href,
        content,
      },
      {
        where: { id: cardId },
      }
    )
      .then((card) => {
        res.json({
          status: 201,
          data: card,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Gak bisa update",
        });
      });
  },

  delete: (req, res) => {
    const cardId = req.params.id;
    Card.destroy({
      where: {
        id: cardId,
      },
    })
      .then((response) => {
        res.send("/cards");
      })
      .catch((err) => {
        res.json({
          status: 400,
          message: "Delete gagal",
        });
      });
  },
};
