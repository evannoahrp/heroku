const router = require("express").Router();
const { index } = require("../controller/homeController");

router.get("/", index);

module.exports = router;
