const router = require("express").Router();
const articleController = require("../controller/articleController");

router.get("/", articleController.index);
router.get("/:id", articleController.show);
router.get("/create", articleController.createArticle);
router.post("/create", articleController.create);
router.patch("/:id", articleController.update);
router.delete("/:id", articleController.delete);

module.exports = router;
