const router = require("express").Router();

const homeRouter = require("./homeRouter");
const articlesRouter = require("./articleRouter");
const cardsRouter = require("./cardRouter");
const usersRouter = require("./userRouter");

router.use("/", homeRouter);
router.use("/articles", articlesRouter);
router.use("/cards", cardsRouter);
router.use("/users", usersRouter);

module.exports = router;
