const router = require("express").Router();

// Middlewares
const restrict = require("../middlewares/restrict");

// Controllers
const userController = require("../controller/userController");

// Register Page
router.get("/register", (req, res) => res.render("register"));
router.post("/register", userController.register);

// Login Page
router.get("/login", (req, res) => res.render("login"));
router.post("/login", userController.login);

// Profile Page
router.get("/whoami", restrict, userController.whoami);

module.exports = router;
